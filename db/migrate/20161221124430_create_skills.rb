class CreateSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :skills do |t|
      t.references :member, foreign_key: true
      t.string :language
      t.integer :level
      t.boolean :need_practise

      t.timestamps
    end
  end
end
