class AddAvailabilityDescriptionToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :availability_description, :text
  end
end
