class RemoveSubtagFromLanguage < ActiveRecord::Migration[5.0]
  def change
    remove_column :languages, :subtag
  end
end
