class CreateLanguages < ActiveRecord::Migration[5.0]
  def change
    create_table :languages do |t|
      t.string :subtag
      t.string :description

      t.timestamps
    end
    add_index :languages, :subtag, unique: true
  end
end
