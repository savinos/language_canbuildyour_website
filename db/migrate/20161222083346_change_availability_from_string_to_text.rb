class ChangeAvailabilityFromStringToText < ActiveRecord::Migration[5.0]
  def change
    change_column :members, :availability,  :text
  end
end
