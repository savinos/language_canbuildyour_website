# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

all_languages_file = File.read(Rails.root.join('lib', 'seeds', 'all_languages.txt'))

languages_fields = all_languages_file.split("%%").grep(/Type: language/)

languages_fields.each do |language|
  fields = language.split("\n")
  description = fields.grep(/Description/)[0].split(" ")[1]
  Language.find_or_create_by(:description => description)
end
