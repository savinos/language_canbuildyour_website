json.extract! skill, :id, :member_id, :language, :level, :need_practise, :created_at, :updated_at
json.url skill_url(skill, format: :json)