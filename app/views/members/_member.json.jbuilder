json.extract! member, :id, :name, :availability, :created_at, :updated_at
json.url member_url(member, format: :json)