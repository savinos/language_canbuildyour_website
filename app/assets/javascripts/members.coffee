# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@paintIt = (element) ->
         k = element.value
         a = document.getElementById("member_availability").value.split(" ")
         if element.checked
             a[k] = 1    
         else
             a[k] = 0
         document.getElementById("member_availability").value = a.join(" ")
