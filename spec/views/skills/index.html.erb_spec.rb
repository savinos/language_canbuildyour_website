require 'rails_helper'

RSpec.describe "skills/index", type: :view do
  before(:each) do
    assign(:skills, [
      Skill.create!(
        :member => nil,
        :language => "Language",
        :level => 2,
        :need_practise => false
      ),
      Skill.create!(
        :member => nil,
        :language => "Language",
        :level => 2,
        :need_practise => false
      )
    ])
  end

  it "renders a list of skills" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Language".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
