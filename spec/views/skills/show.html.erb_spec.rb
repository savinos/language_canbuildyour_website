require 'rails_helper'

RSpec.describe "skills/show", type: :view do
  before(:each) do
    @skill = assign(:skill, Skill.create!(
      :member => nil,
      :language => "Language",
      :level => 2,
      :need_practise => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Language/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/false/)
  end
end
