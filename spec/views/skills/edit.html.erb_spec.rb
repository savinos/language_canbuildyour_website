require 'rails_helper'

RSpec.describe "skills/edit", type: :view do
  before(:each) do
    @skill = assign(:skill, Skill.create!(
      :member => nil,
      :language => "MyString",
      :level => 1,
      :need_practise => false
    ))
  end

  it "renders the edit skill form" do
    render

    assert_select "form[action=?][method=?]", skill_path(@skill), "post" do

      assert_select "input#skill_member_id[name=?]", "skill[member_id]"

      assert_select "input#skill_language[name=?]", "skill[language]"

      assert_select "input#skill_level[name=?]", "skill[level]"

      assert_select "input#skill_need_practise[name=?]", "skill[need_practise]"
    end
  end
end
