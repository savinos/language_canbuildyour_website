require 'rails_helper'

RSpec.describe "skills/new", type: :view do
  before(:each) do
    assign(:skill, Skill.new(
      :member => nil,
      :language => "MyString",
      :level => 1,
      :need_practise => false
    ))
  end

  it "renders new skill form" do
    render

    assert_select "form[action=?][method=?]", skills_path, "post" do

      assert_select "input#skill_member_id[name=?]", "skill[member_id]"

      assert_select "input#skill_language[name=?]", "skill[language]"

      assert_select "input#skill_level[name=?]", "skill[level]"

      assert_select "input#skill_need_practise[name=?]", "skill[need_practise]"
    end
  end
end
