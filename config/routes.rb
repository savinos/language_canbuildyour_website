Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'users/sessions' }
  root 'static_pages#home'

  resources :members do
    resources :skills
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
