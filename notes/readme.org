#+TODO: TODO INPROGRESS | DONE CANCELED
* Logbook 01/12
** DONE fix the bug with assets
   :LOGBOOK:
   CLOCK: [2017-01-12 Thu 09:31]--[2017-01-12 Thu 09:34] =>  0:03
   :END:
** DONE change the name of repository
   :LOGBOOK:
   CLOCK: [2017-01-12 Thu 10:36]--[2017-01-12 Thu 10:56] =>  0:20
   :END:
** DONE add htaccess and robots in rails
   :LOGBOOK:
   CLOCK: [2017-01-12 Thu 11:11]--[2017-01-12 Thu 11:29] =>  0:18
   :END:
   - [X] htaccess
   - [X] robots
** DONE initialize cucumber and rspec
   :LOGBOOK:
   CLOCK: [2017-01-12 Thu 11:37]--[2017-01-12 Thu 11:43] =>  0:06
   :END:
** DONE add algorithm with groups
   :LOGBOOK:
   CLOCK: [2017-01-12 Thu 12:21]--[2017-01-12 Thu 13:21] =>  1:00
   :END:
   - [ ] pencil and paper
   - [ ] write cucumber
* Backlog
** TODO testing
   - cucumber
   - rspec
** TODO generate groups
   - [ ] write rspec
   - [ ] task of server
