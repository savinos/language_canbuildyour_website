Feature: Generate Groups
  
  Scenario: Generate Groups With Compatible Hours and Same Level
    Given the following members:
    | Name               | Availability description                      | Skills                                             |
    | Savvas Alexandrou  | Mon after 6pm                                 | Greek-native English-B2-practise, German-A2        |
    | Natalie Hami       | Most days after 5pm                           | English-native Greek-native Turkish-C2-p           |
    | Argyro Nicolaou    | Tue, Wed, Fri any time. Mon, Thu after 7pm    | Turkish-B2-p English-C1 Greek-native French-A2     |
    | Hazel Sabanlar     | Tue, Sat afternoon, Fridays after 18.30m, Sun | Greek-C1-p Turkish-native                          |
    | Maria Kile         | Mon or Fri after 6pm                          | Turkish-A1-p Greek-native                          |
    | Rifat Kulak        | Flexible                                      | Greek-A1-p Turkish-native English-C1-p             |
    | Melike Kalkan      | Weekdays after 6pm                            | Greek-A1-p Turkish-native English-C1 Portuguese-A1 |
    | Burc Barin         | Weekdays 6-8pm                                | Turkish-native English-C1 Greek-A1-p               |
    | Evgenia Dermitzaki | Tue  3-5                                      | Turkish-A2-p Greek-native                          |
    | Christos Cassianos | Fridays Saturdays Sundays                     | Turkish-A1-p English-C1 Greek-native               |
    | Moses Elmadjian    | Weedays afternoons                            | English-C1 Greek-C1 Trurkish-C1-p Armenian-C1      |
    | Lena Harman        | Weekdays after 5pm                            | Turkish-B1-p English-B1-p                          |
    | Zozo Evripidou     | Weekdays afternoon                            | Greek-native English-B1-p                          |
    When I follow "Groups"
    Then I should see the following groups
    
